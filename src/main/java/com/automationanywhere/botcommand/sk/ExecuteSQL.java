/*
 * Copyright (c) 2020 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 * 
 */


/**
 * @author Stefan Karsten
 *
 */

package com.automationanywhere.botcommand.sk;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.sk.utils.IBMDB2Connection;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;



/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Execute SQL", name = "ExecuteSQL", description = "Execute SQL", 
icon = "pkg.svg", node_label = "Execute SQL{{sessionName}}", comment = true ,  text_color = "#413e73" , background_color =   "#413e73" ,
return_type=DataType.LIST , return_sub_type = STRING , return_label="Status", return_required=true)

public class ExecuteSQL{
 
    @Sessions
    private Map<String, Object> sessions;
    
    @Execute
    public ListValue<String> action(@Idx(index = "1", type = TEXT) @Pkg(label = "Session Name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
    								@Idx(index = "2", type = TEXT) @Pkg(label = "SQL Statement",  default_value_type = STRING) @NotEmpty String statement)
    								throws Exception {
 

    	IBMDB2Connection connection  = (IBMDB2Connection) this.sessions.get(sessionName);  
    	
    	List<String> result = connection.executeSQL(statement);
    	
    	ListValue returnValue = new ListValue<String>();
    	returnValue.set(result);

        return returnValue;


    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
    
 
    
    
}
