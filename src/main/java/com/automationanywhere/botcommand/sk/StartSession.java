/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.bot.service.GlobalSessionContext;

import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.sk.utils.IBMDB2Connection;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.core.security.SecureString;




/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start Session", name = "StartDB2BSession", description = "Start DB2 session", 
icon = "pkg.svg", comment = true ,  text_color = "#413e73"  , background_color = "#413e73"   ,node_label = "Start Session {{sessionName}}") 
public class StartSession {
 
    @Sessions
    private Map<String, Object> sessions;
    
    
	private static final Messages MESSAGES = MessagesFactory
			.getMessages("com.automationanywhere.botcommand.samples.messages");
    
	  
	@com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
	  
	private IBMDB2Connection connection;
	
    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session Name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "DB2 URL", default_value_type = STRING) @NotEmpty String url,
            @Idx(index = "3", type = CREDENTIAL) @Pkg(label = "User", default_value_type = STRING) @NotEmpty SecureString user,
            @Idx(index = "4", type = CREDENTIAL) @Pkg(label = "Password" , default_value_type = STRING) @NotEmpty SecureString password,
            @Idx(index = "5", type = AttributeType.BOOLEAN) @Pkg(label = "Auto Commit" , default_value_type = DataType.BOOLEAN , default_value = "true") @NotEmpty Boolean commit) throws Exception
            {
 
        // Check for existing session
        if (sessions.containsKey(sessionName))
            throw new BotCommandException(MESSAGES.getString("Session name in use")) ;
        
        // Create a ConnectionFactory
 		

	    this.connection = new IBMDB2Connection(url, user.getInsecureString(), password.getInsecureString(),commit);
	    this.sessions.put(sessionName, this.connection);
        

    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
    
 
    
    
}