package com.automationanywhere.botcommand.sk.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;


public class IBMDB2Connection {

	
	private Connection con;
	private String accesskey;
	private String secretkey;
	
	public  IBMDB2Connection(String url,String user, String password,Boolean commit) throws Exception {
		  Class.forName("com.ibm.db2.jcc.DB2Driver");            
	      System.out.println("**** Loaded the JDBC driver");

	      // Create the connection using the IBM Data Server Driver for JDBC and SQLJ
	      con = DriverManager.getConnection (url, user, password);  
	      // Commit changes manually
	      con.setAutoCommit(commit);
	      System.out.println("**** Created a JDBC connection to the data source");
	}
	

	
	public void close() throws Exception {
		con.close(); 
	}
	
	
	public Connection getDBconnection() {
		return con;
	}
	
	
	public List<String>  executeSQL(String statement) throws Exception {
		
		List<String> result = new ArrayList<String>();

		Statement stmt = con.createStatement();         
	    System.out.println("**** Created JDBC Statement object");
	    
	     ResultSet rs = stmt.executeQuery(statement);    
	    
	    ResultSetMetaData metaData = rs.getMetaData();
        int colCount = metaData.getColumnCount();

	      // Execute a query and generate a ResultSet instance
	     while (rs.next()) {
	    	 for (int i = 0; i < colCount; i++) {
	    		 result.add(rs.getString(i+1));
             } 
	     }
	     rs.close();
	     stmt.close();
         con.commit();
	     return result;
	}
	
	
	
	public Boolean  insertSQL(String table, LinkedHashMap<String,Value> data) throws Exception {
		
		boolean result = false;



	      // Execute a query and generate a ResultSet instance
		
		String statement = "INSERT INTO "+table+" (";
		String valueStatement =" VALUES(";
		int counter = data.size();
		for (Entry<String,Value> column: data.entrySet()) {
			String name = column.getKey();
			String value = column.getValue().get().toString();
			statement = statement+name;
			valueStatement = valueStatement+"'"+value+"'";
			counter--;
			if (counter>0 ) {
				statement = statement + ", ";
				valueStatement = valueStatement + ", ";
			}
			else {
				statement = statement + ")";
				valueStatement = valueStatement + ")";
			}
		}
		
		statement = statement + valueStatement;
		System.out.println(statement);
		
		PreparedStatement stmt3 = con.prepareStatement(statement);
		
	
		
		result = stmt3.execute();
		stmt3.close();
		
		con.commit();
	
        return  result;
	}
	
	
		







	
	
}
